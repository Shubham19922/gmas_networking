import Constants from "./Constants";
import {AsyncStorage} from 'react-native';
import {Platform} from 'react-native';
import SQLite from 'react-native-sqlite-storage'

class Api {


    constructor() {

        sesssionId = null;
        socialSessionId = null;
        csrf = null;

        finalSession = null;

        AsyncStorage.setItem('sessionId', null);


    }

    static saveSession(id) {

        let key = 'sessionId';


        console.log("saveSession k andar", id)

        global.storage.save({
            key: 'sessionState',
            data: {
                session: id
            }
        })

        // try {
        //     await AsyncStorage.setItem(key , id);
        //     console.log("ASYNC", AsyncStorage);
        // } catch (error) {
        //     console.log(error);
        // }


    }

    static saveSessionSocialSignup(id) {

        let key = 'sessionIdSocial';


        console.log("saveSession social k andar", id)


        global.storage.save({
            key: 'sessionIdSocial',
            data: {
                session: id
            }
        })

    }


    static async getFinalSession() {

        console.log('final session k andar')
        try {
            console.log("try block k andar")
            const value = await AsyncStorage.getItem('sessionId');
            this.finalSessionId = value;
            console.log("sid", value);
        } catch (error) {
            console.log(error)
        }


    }


    static setCsrf(csrf) {
        console.log("Save karne se pehle", this.csrf)
        this.csrf = csrf;
        console.log("save karne ke baad", this.csrf)
        global.csrf = csrf
        console.log("global csrf", global.csrf)

        global.storage.save({
            key: 'csrf',
            data: {
                csrf: csrf
            }
        })
    }

    static getCsrf(csrf) {
        return this.csrf;
    }

    static setSessionId(sessionId) {
        this.sessionId = sessionId;
        global.sessionId = sessionId
    }

    static setSocialSessionId(sessionId) {
        this.socialSessionId = sessionId;
        global.socialSessionId = sessionId
    }

    static getSessionId(sessionId) {
        return this.sessionId;
    }

    static getSocialSessionId(sessionId) {
        return this.socialSessionId;
    }

    static headersJSON() {

        if (this.getSessionId() && this.getSessionId() != null) {
            console.log("Global vala", global.csrf)
            return {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                //'X-CSRFToken': global.csrf,
                'X-Requested-From': 'enduser-app',
                'Cookie': 'sessionid=' + global.sessionId + ';',
                'X-Requested-With': 'XMLHttpRequest',
                'GMAS-AGENT': Platform.OS === 'ios' ? 'ios' : 'android',
            }
        }
        else {
            console.log("Global vala", global.csrf)
            return {

                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRFToken': global.csrf,
                'X-Requested-With': 'XMLHttpRequest',
                'GMAS-AGENT': Platform.OS === 'ios' ? 'ios' : 'android',
            }
        }
    }


    static headersWithoutCSRF() {
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
        }

    }

    static headersFormData() {
        console.log("Global vala", global.csrf)
        if (this.getSocialSessionId()) {
            return {
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-CSRFToken': global.csrf,
                'X-Requested-With': 'XMLHttpRequest',
                //'Cookie': "sessionid=" + global.socialSessionId,
                'session_id': global.socialSessionId,
                'X-Requested-From': Constants.USERNAME,
                'Referer': Constants.BASE_URI,
                'GMAS-AGENT': Platform.OS === 'ios' ? 'ios' : 'android',
            }
        } else if (this.getSessionId()) {
            return {
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-CSRFToken': global.csrf,
                'X-Requested-With': 'XMLHttpRequest',
                'Cookie': "sessionid=" + global.sessionId,
                'X-Requested-From': Constants.USERNAME,
                'Referer': Constants.BASE_URI,
                'GMAS-AGENT': Platform.OS === 'ios' ? 'ios' : 'android',
            }
        }
        else {
            console.log("Global vala", global.csrf)
            return {
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-CSRFToken': global.csrf,
                'X-Requested-With': 'XMLHttpRequest',
                'X-Requested-From': Constants.USERNAME,
                'Referer': Constants.BASE_URI,
                'GMAS-AGENT': Platform.OS === 'ios' ? 'ios' : 'android',
            }
        }
    }

    static headersMultipartData() {
        return {
            'Content-Type': 'multipart/form-data',
            'X-CSRFToken': global.csrf,
            'X-Requested-With': 'XMLHttpRequest',
            'X-Requested-From': Constants.USERNAME

        }
    }

    static prepareHeadersFromLocalStorage(dataType) {

    }

    static get(route) {
        return this.xhr(route, null, 'GET', Constants.JSON_DATA);
    }

    static getCSRF(route) {
        return this.xhr(route, null, 'GET', Constants.WITHOUT_CSRF);
    }

    static put(route, params, datatype) {
        return this.xhr(route, params, 'PUT', datatype)
    }

    static putForWishlist(route, params, datatype) {
        return this.xhrForWishlist(route, params, 'PUT', datatype)
    }

    static post(route, params, dataType) {
        return this.xhr(route, params, 'POST', dataType)
    }

    static delete(route, params, datatype) {
        return this.xhr(route, params, 'DELETE')
    }

    static xhr(route, params, verb, datatype) {
        const host = Constants.BASE_URI;
        const url = `${host}${route}`;
        let options = Object.assign({method: verb}, params ? {body: params} : null);
        options.headers = Api.headersJSON();
        if (datatype === Constants.WITHOUT_CSRF)
            options.headers = Api.headersWithoutCSRF();
        if (datatype === Constants.FORM_DATA)
            options.headers = Api.headersFormData();
        if (datatype === Constants.MULTIPART_DATA)
            options.headers = Api.headersMultipartData();
        console.log(url, options);
        let data;

        try {
            return fetch(url, options)
                .then(resp => {

                    console.log("api raw response ==>", resp);

                    let setCookie = "set-cookie"
                    if (resp.headers.map[setCookie]) {
                        console.log("csrf condition")
                        let csrfString = resp.headers.map[setCookie][0];

                        // save csrf only if its csrf and not sessionid
                        if (csrfString.indexOf("csrf") != -1) {
                            let csrfToken = csrfString.split(";")
                            let csrf = csrfToken[0].split('=')
                            let finalCsrf = csrf[1]
                            // let indexCSRF = setCookieString.indexOf('; expires') - 10;
                            // let csrf = setCookieString.substr(10, indexCSRF);
                            this.setCsrf(finalCsrf);


                        }
                        //save sessionid from login response
                        if (url.indexOf("login") != -1
                            || url.indexOf("mobile-social-signup-complete") != -1
                        ) {
                            let sessionIdString = resp.headers.map[setCookie][0];
                            console.log("sessionId condtion ", sessionIdString)
                            if (sessionIdString.indexOf("sessionid") != -1) {

                                let sessionIdToken = sessionIdString.split(";")
                                let sessionId = sessionIdToken[0].split('=')
                                let finalSessionId = sessionId[1]
                                // let indexSessionId = setCookieString.indexOf('; expires') - 10;
                                // let sessionId = setCookieString.indexOf('; expires') - 10;
                                this.setSessionId(finalSessionId);

                                this.saveSession(this.getSessionId());

                            }
                        }


                        if (url.indexOf("mobile-social-auth-facebook") != -1
                            || url.indexOf("mobile-social-auth-google") != -1) {
                            let sessionIdString = resp.headers.map[setCookie][0];
                            console.log("sessionId condtion ", sessionIdString)
                            if (sessionIdString.indexOf("sessionid") != -1) {

                                let sessionIdToken = sessionIdString.split(";")
                                let sessionId = sessionIdToken[0].split('=')
                                let finalSessionId = sessionId[1]
                                // let indexSessionId = setCookieString.indexOf('; expires') - 10;
                                // let sessionId = setCookieString.indexOf('; expires') - 10;

                                console.log("response new", resp)
                                this.setSessionId(finalSessionId);

                                this.saveSession(this.getSessionId());

                                this.setSocialSessionId(finalSessionId);

                                this.saveSessionSocialSignup(this.getSessionId());

                            }
                        }

                        // //Don't convert to json for post address response
                        // if (url.indexOf("user/address/")!=-1){
                        //     console.log("Apis ke ek if ke ander")
                        //     return resp;
                        // }
                        // console.log("if ke bahar")
                    }
                    return resp.json().then(actualResponse => {
                        console.log("actual response ", actualResponse);
                        console.log("sessionId", this.getSessionId());


                        this.getFinalSession();

                        if (url.indexOf("category") != -1) {
                            return actualResponse;
                        }

                        if (url.indexOf("product") != -1) {
                            return actualResponse
                        }

                        if (actualResponse.objects) {
                            return actualResponse.objects;
                        } else if (actualResponse.results) {
                            return actualResponse.results;
                        } else {
                            return actualResponse;
                        }

                    })


                })
                .catch(err => {
                    console.log("api error ", err);
                });
        } catch (e) {
            console("exception api");
        }
    }


    static xhrForWishlist(route, params, verb, datatype) {
        const host = Constants.BASE_URI;
        const url = `${host}${route}`;
        let options = Object.assign({method: verb}, params ? {body: params} : null);
        options.headers = Api.headersJSON();
        if (datatype === Constants.WITHOUT_CSRF)
            options.headers = Api.headersWithoutCSRF();
        if (datatype === Constants.FORM_DATA)
            options.headers = Api.headersFormData();
        if (datatype === Constants.MULTIPART_DATA)
            options.headers = Api.headersMultipartData();
        console.log(url, options);
        let data;
        var json = JSON.stringify(params)

        return new Promise((resolve, reject) => {
            var xhr = new XMLHttpRequest();
            xhr.open("PUT", url, true);
            xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
            xhr.setRequestHeader('X-Requested-From', 'enduser-app')
            xhr.setRequestHeader('Cookie', 'sessionid=' + global.sessionId + ';')
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
            xhr.setRequestHeader('GMAS-AGENT', Platform.OS === 'ios' ? 'ios' : 'android')

            xhr.onload = function () {
                if (xhr.readyState == 4 && xhr.status == "200") {
                    console.log(xhr.responseText);
                    resolve("success")
                } else {
                    reject(xhr.responseText)
                }
            }
            xhr.onerror = () => reject(xhr.statusText);
            xhr.send(json);
        });
    }
}

export default Api