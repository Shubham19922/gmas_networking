export  default Constants = {
    USERNAME : "alexjewellery",
    APP_NAME : "Alex Jewellery",
    PRIMARY_COLOR:'#3c0e28',
    SECONDARY_COLOR:'red',
    BASE_URI:'http://alexjewellery.getmeashop.com/',
    BASE_URI_INVENTORY:'https://api.getmeashop.com/',
    JSON_DATA:'json',
    FORM_DATA:'formData',
    MULTIPART_DATA:'multipartData'
}
